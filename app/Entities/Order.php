<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_name', 'user_phone', 'user_email', 'amount', 'status' 
    ];

    public function cartitems(){
    	return $this->hasMany('App\Entities\Cartitem');
	}

	public function carts(){
    	return $this->hasManyThrough('App\Entities\Cart', 'App\Entities\Cartitem');
	}

}
