<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name', 'description', 'price', 'category_id' 
    ];
    
    public function category(){
    	return $this->belongsTo('App\Entities\Service');
	}
}
