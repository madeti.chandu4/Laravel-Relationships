<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'description' 
    ];

    public function service(){
    	return $this->hasMany('App\Entities\Service');
	}
}
