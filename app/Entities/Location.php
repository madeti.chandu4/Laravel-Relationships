<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'name', 'street_name', 'locality', 'city', 'state', 'zip', 'lat', 'lng' 
    ];
}
