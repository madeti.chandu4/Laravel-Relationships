<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cartitem extends Model
{
    protected $fillable = [
        'user_id', 'order_id', 'service_id', 'quantity' 
    ];
}
