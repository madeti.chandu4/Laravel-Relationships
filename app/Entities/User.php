<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'phone', 'email' 
    ];
    public function location(){
    	return $this->hasMany('App\Entities\Location');
	}
	public function service(){
    	return $this->hasMany('App\Entities\Service');
	}
	public function documents(){
    	return $this->hasMany('App\Entities\Document');
	}
    public function role(){
        return $this->belongsTo('App\Entities\Role');
    }
}
