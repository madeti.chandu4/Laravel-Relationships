<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'user_id', 'service_id', 'quantity' 
    ];
    public function service(){
    	return $this->belongsTo('App\Entities\Service');
	}

	public function user(){
    	return $this->belongsTo('App\Entities\User');
	}
}
