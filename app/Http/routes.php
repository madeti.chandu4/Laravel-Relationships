<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(array('prefix' => 'api/v1', 'middleware' => ['web']), function()
{
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

    
    //Create User APIS
    
    Route::post('/user', 'UsersController@create');
    Route::post('/user/{id}', 'UsersController@update');
    Route::get('/user/{id}', 'UsersController@getUserByID');
    Route::get('/users', 'UsersController@getAllUsers');
    Route::delete('/user/{id}', 'UsersController@delete');
    Route::get('/users/role/{role_id}', 'UsersController@getUsersByRole');
    Route::get('/role/user/{user_id}', 'UsersController@getRoleByUser');
    
    
    
    //Location APIS

    Route::post('/location', 'LocationsController@create');
    Route::post('/location/{id}', 'LocationsController@update');
    Route::get('/location/{id}', 'LocationsController@getLocationByID');
    Route::get('/locations', 'LocationsController@getAllLocations');
    Route::delete('/location/{id}', 'LocationsController@delete');
    Route::get('/location/user/{user_id}', 'LocationsController@getLocationForUser');

    Route::post('/category', 'CategoriesController@create');
    Route::post('/category/{id}', 'CategoriesController@update');
    Route::get('/category/{id}', 'CategoriesController@getCategoryByID');
    Route::get('/categories', 'CategoriesController@getAllCategories');
    Route::delete('/category/{id}', 'CategoriesController@delete');
    Route::get('/category/service/{service_id}', 'CategoriesController@getCategoryForService');


    Route::post('/service', 'ServicesController@create');
    Route::post('/service/{id}', 'ServicesController@update');
    Route::get('/service/{id}', 'ServicesController@getServiceByID');
    Route::get('/services', 'ServicesController@getAllServices');
    Route::delete('/service/{id}', 'ServicesController@delete');
    Route::get('/service/category/{category_id}', 'ServicesController@getServiceByCategory');
    Route::get('/service/user/{user_id}', 'ServicesController@getServicesForUser');
    Route::get('/user/service/{service_id}', 'ServicesController@getUserByService');

    Route::post('/document', 'DocumentsController@create');
    Route::post('/document/{id}', 'DocumentsController@update');
    Route::get('/document/{id}', 'DocumentsController@getDocumentByID');
    Route::get('/documents', 'DocumentsController@getAllDocuments');
    Route::delete('/document/{id}', 'DocumentsController@delete');
    Route::get('/document/user/{id}', 'DocumentsController@getDocumetsForUser');

    Route::get('/cartitem/order/{order_id}', 'OrdersController@getCartitemsByOrder');
});
