<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\Order;
use App\Entities\Service;

use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;

class OrdersController extends Controller
{
    public function getCartitemsByOrder($order_id){
    	$order = Order::find($order_id);
    	if($order){
    		$cartitems =$order->cartitems()->get();
    		$jsend = JSendResponse::success($cartitems->toArray());
    	}else{
    		$message = JSendResponse::fail(['message' => 'Couldnt find service']);
        	return response($message, 401);
      	}
      	return $jsend;
	}
}
