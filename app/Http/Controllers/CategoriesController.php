<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\Category;
use App\Entities\Service;

use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;
class CategoriesController extends Controller
{
    public function create(){

    	$input = Input::all();
    	$validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
  			$message = JSendResponse::fail(['message' => 'Validaion error', 'errors' => $validator->messages()]);
        	return response($message, 401);
       	}
        
        $created = $category = new Category;
       	$category->name = Input::get('name');
       	$category->description = Input::get('description');
       	$category->save();

       	$success = JSendResponse::success(['message' => "New category has been created", 'id' => $created->id]);
        return $success;
    }

    

    public function update($id){
        $category = Category::where('id', $id)->find($id);
        $category->name = Input::get('name');
       	$category->description = Input::get('description');
       	$category->update();
        $categoryupdated = $category->save();
        if($categoryupdated){
            $message = JSendResponse::success(['message' => 'Category Successfully Updated', 'input'=>Input::all()]);
        }else{
            $message = JSendResponse::fail(['message' => 'Category Couldnt be Updated']);
            return response($message, 401);
        }
        return $message;
	}

    
    public function getCategoryByID($id){
      $category = Category::find($id);
      $jsend = JSendResponse::success($category->toArray());
      return $jsend;
    }

    public function getAllCategories(){
		$categories = Category::all();
      	$jsend = JSendResponse::success($categories->toArray());
      	return $jsend;
    }

    public function delete($id){
     	$category = Category::where('id', $id)->first();
     	if($category){
           $categorydeleted = $category->delete($id);
       		if($categorydeleted){
          		$success = JSendResponse::success(['message' => 'category deleted successfully', 'input'=>Input::all()]);
       		}
       		return $success;
     	}
    }

    public function getCategoryForService($service_id){
    	$service = Service::find($service_id);
    	if($service){
    		$category =$service->category()->get();
    		$jsend = JSendResponse::success($category->toArray());
    	}else{
    		$message = JSendResponse::fail(['message' => 'Couldnt find service']);
        	return response($message, 401);
      	}
      	return $jsend;
	}
}
