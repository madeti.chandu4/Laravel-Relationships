<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\Location;
use App\Entities\Userlocation;
use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;

class LocationsController extends Controller{
  public function create(){
  	  $input = Input::all();
  	  $Validator = Validator::make($input, [
        'name' => 'required',
        'street_name' => 'required',
	      'locality' => 'required',
	      'city' => 'required',
	      'state' => 'required',
	      'zip' => 'required',
	      'lat' => 'required',
	      'lng' => 'required'
      ]);
      if($Validator->fails()){
      	$error = JSendResponse::fail(['message' => 'Validator error', 'errors' => $Validator->messages()]);
      	return $error;
      }
      $created = $location = new Location;
      $location->name = Input::get('name');
      $location->street_name = Input::get('street_name');
      $location->locality = Input::get('locality');
      $location->city = Input::get('city');
      $location->state = Input::get('state');
      $location->zip = Input::get('zip');
      $location->lat = Input::get('lat');
      $location->lng = Input::get('lng');
      $location->save();
      if($created){
        $success = JSendResponse::success(['message' => 'new location has been created', 'id' => $created->id]);
        return $success;
      }else{
        $error = JSendResponse::error(['message' => 'Error! A new city could not be created', 401, $data]);
        return $error;
      }
  }


  public function update($id){
    $location = Location::where('id', $id)->first();
    $location->location_name = Input::get('name');
    $location->street_name = Input::get('street_name');
    $location->locality = Input::get('locality');
    $location->city = Input::get('city');
    $location->state = Input::get('state');
    $location->zip = Input::get('zip');
    $location->lat = Input::get('lat');
    $location->lng = Input::get('lng');
    $location->update();
    $locationupdated = $location->save();
    if($locationupdated){
      $message = JSendResponse::success(['message' => 'location Sucessfully Updated', 'input'=>Input::all()]);
      return $message;
    }else{
      $error = JSendResponse::error(['message' => 'location couldnot be Updated', 401, $data]);
      return $error;
    }
  }
  
    public function getLocationByID($id){
      $location = Location::find($id);
      $Jsend = JSendResponse::success($location->toArray());
      return $Jsend;
    }
    public function getAllLocations(){
    	$location = Location::all();
    	$Jsend = JSendResponse::success($location->toArray());
    	return $Jsend;
    }

	public function delete($id){
	    $location = Location::where('id', $id)->first();
	    if($location){
	      $locationdeleted = $location->delete($id);
	      if($locationdeleted){
	        $success = JSendResponse::success(['message' => 'location deleted sucessfully', 'input'=>Input::all()]);
	      }
	      return $success;
	    }
	}

	public function getLocationForUser($user_id){
      $user = Userlocation::find($user_id);
      if($user){
        $location =$user->location()->get();
        $jsend = JSendResponse::success($location->toArray());
      }else{
        $message = JSendResponse::fail(['message' => 'Couldnt find user']);
          return response($message, 401);
      }
      return $jsend;
    }  
}
