<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\Service;
use App\Entities\Cart;
use App\Entities\Category;
use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;

class ServicesController extends Controller
{
    public function create(){

    	$input = Input::all();
    	$validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
  			$message = JSendResponse::fail(['message' => 'Validaion error', 'errors' => $validator->messages()]);
        	return response($message, 401);
       	}
        $created = $service = new Service;
       	$service->name = Input::get('name');
       	$service->description = Input::get('description');
       	$service->service = Input::get('price');
       	$service->service = Input::get('category_id');
       	$category->save();
		$success = JSendResponse::success(['message' => "New Service has been created", 'id' => $created->id]);
        return $success;
    }

    

    public function update($id){
        $service = Service::where('id', $id)->find($id);
        $service->name = Input::get('name');
       	$service->description = Input::get('description');
       	$service->service = Input::get('price');
       	$service->service = Input::get('category_id');
       	$service->update();
        $serviceupdated = $service->save();
        if($serviceupdated){
            $message = JSendResponse::success(['message' => 'Service Successfully Updated', 'input'=>Input::all()]);
        }else{
            $message = JSendResponse::fail(['message' => 'Service Couldnt be Updated']);
            return response($message, 401);
        }
        return $message;
	}

    
    public function getServiceByID($id){
      	$service = Service::find($id);
      	$jsend = JSendResponse::success($service->toArray());
      	return $jsend;
    }

    public function getAllServices(){
		$services = Service::all();
      	$jsend = JSendResponse::success($services->toArray());
      	return $jsend;
    }

    public function delete($id){
     	$service = Service::where('id', $id)->first();
     	if($service){
           $servicedeleted = $service->delete($id);
       		if($servicedeleted){
          		$success = JSendResponse::success(['message' => 'Service deleted successfully', 'input'=>Input::all()]);
       		}
       		return $success;
     	}
    }
    //Service belongs to category API
    public function getServiceByCategory($category_id){
    	$category = Category::find($category_id);
    	if($category){
    		$service =$category->service()->get();
    		$jsend = JSendResponse::success($service->toArray());
    	}else{
    		$message = JSendResponse::fail(['message' => 'Couldnt find category']);
        	return response($message, 401);
      	}
      	return $jsend;
	}
    public function getUserByService($service_id){
    	$service = Cart::find($service_id);
      	if($service){
        	$user =$service->user()->get();
        	$jsend = JSendResponse::success($user->toArray());
      	}else{
        	$message = JSendResponse::fail(['message' => 'Couldnt find service']);
          	return response($message, 401);
        }
        return $jsend;
    }
    
	public function getServicesForUser($user_id){
      	$user = Cart::find($user_id);
      	if($user){
        	$service =$user->service()->get();
        	$jsend = JSendResponse::success($service->toArray());
      	}else{
        	$message = JSendResponse::fail(['message' => 'Couldnt find user']);
          	return response($message, 401);
        }
        return $jsend;
    }
}
