<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\Document;
use App\Entities\User;

use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;
class DocumentsController extends Controller
{
    public function create()
    {

    	$input = Input::all();
    	$validator = Validator::make($input, [
            'type' => 'required',
            'link' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
  			$error = JSendResponse::fail(['message' => 'Validaion error', 'errors' => $validator->messages()]);
        	return $error;
       	}
        $created = $document = new Document;
        $document->type = Input::get('type');
       	$document->link = Input::get('link');
       	$document->name = Input::get('name');
       	$document->description = Input::get('description');
       	$document->save();
		$success = JSendResponse::success(['message' => "New Document has been created", 'id' => $created->id]);
        return $success;
    }

    

    public function update($id){
        $document = Document::where('id', $id)->find($id);
        $document->type = Input::get('type');
       	$document->link = Input::get('link');
       	$document->name = Input::get('name');
       	$document->description = Input::get('description');
       	$document->update();
        $documentupdated = $document->save();
        if($documentupdated){
            $message = JSendResponse::success(['message' => 'Document Successfully Updated', 'input'=>Input::all()]);
        }else{
            $message = JSendResponse::fail(['message' => 'Document Couldnt be Updated']);
            return response($message, 401);
        }
        return $message;
	}

    
    public function getDocumentByID($id){
      $document = Document::find($id);
      $jsend = JSendResponse::success($document->toArray());
      return $jsend;
    }

    public function getAllDocuments(){
		$documents = Document::all();
     	$jsend = JSendResponse::success($documents->toArray());
      	return $jsend;
    }

    public function delete($id){
     	$document = Document::where('id', $id)->first();
     	if($document){
           $documentdeleted = $document->delete($id);
       		if($documentdeleted){
          		$success = JSendResponse::success(['message' => 'Document deleted successfully', 'input'=>Input::all()]);
       		}
       		return $success;
     	}
    }
    public function getDocumetsForUser($user_id){
    	$user = User::find($user_id);
    	if($user){
    		$documents =$user->documents()->get();
    		$jsend = JSendResponse::success($documents->toArray());
    	}else{
    		$message = JSendResponse::fail(['message' => 'Couldnt find user']);
        	return response($message, 401);
      	}
      	return $jsend;
	}
}
