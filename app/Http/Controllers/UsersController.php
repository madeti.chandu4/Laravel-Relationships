<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Response\JSendResponse;
use App\Entities\User;
use App\Entities\Location;
use App\Entities\Role;
use Auth;
use JWTAuth;
use stdClass;
use App\Http\Requests;

class UsersController extends Controller
{

    ///Create a new User 
    public function create(){

    	  $input = Input::all();
    	  $validator = Validator::make($input, [
          'name' => 'required',
          'phone' => 'required|min:10',
          'email' => 'required|email',
          'role_name' => 'required'  //string
        ]);
        if ($validator->fails()) {
  			$message = JSendResponse::fail(['message' => 'Validaion error', 'errors' => $validator->messages()]);
        	return response($message, 401);
       	}
        $role_name = Input::get("role_name");
        $role = Role::where('name', '=', $role_name)->first();
        $created = $user = new User;
       	$user->name = Input::get('name');
       	$user->phone = Input::get('phone');
       	$user->email = Input::get('email');
       	if($role){
          $user->role_id = $role->id;
        }else{
          $message = "Role name doesnt exist";
          return response($message, 401);
        }
        $user->save();
        $success = JSendResponse::success(['message' => "New user has been created", 'id' => $created->id]);
        return $success;
    }

    

    public function update($id){
        $user = User::where('id', $id)->find($id);
        $user->name = Input::get('name');
        $user->phone = Input::get('phone');
        $user->email = Input::get('email');
        $user->role_id = Input::get('role_id');
        $user->update();
        $userupdated = $user->save();
        if($userupdated){
            $message = JSendResponse::success(['message' => 'User Successfully Updated', 'input'=>Input::all()]);
        }else{
            $message = JSendResponse::fail(['message' => 'User Couldnt be Updated']);
            return response($message, 401);
        }
        return $message;
    }

    
    public function getUserByID($id){
      $user = User::find($id);
      $jsend = JSendResponse::success($user->toArray());
      return $jsend;
    }

    public function getAllUsers(){
      $users = User::all();
      $jsend = JSendResponse::success($users->toArray());
      return $jsend;
    }

    public function delete($id){
     $user = User::where('id', $id)->first();
     if($user){
           $userdeleted = $user->delete($id);
       if($userdeleted){
          $success = JSendResponse::success(['message' => 'user deleted successfully', 'input'=>Input::all()]);
       }
       return $success;
     }
    }
    //Role has Many users
    public function getUsersByRole($role_id){
      $role = Role::find($role_id);
      if($role){
        $user = $role->user()->get();
        $jsend = JSendResponse::success($user->toArray());
        }else{
          $message = JSendResponse::fail(['message' => 'Couldnt find role']);
          return response($message, 401);
      }
      return $jsend;
    }
    //user belongs to role
    public function getRoleByUser($user_id){
      $user = User::find($user_id);
      if($user){
        $role = $user->role()->get();
        $jsend = JSendResponse::success($role->toArray());
        }else{
          $message = JSendResponse::fail(['message' => 'Couldnt find user']);
          return response($message, 401);
      }
      return $jsend;
    }
}